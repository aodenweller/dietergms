# A Dispatch and Investment Evaluation Tool with Endogenous Renewables "DIETER" 

version 1.5.0-pre
​
-----

The Dispatch and Investment Evaluation Tool with Endogenous Renewables (DIETER) has initially been developed to study the role of electricity storage and other flexibility options in greenfield scenarios with high shares of variable renewable energy sources. The model minimizes overall system costs in a long-run equilibrium setting, determining least-cost capacity expansion and use of various generation and flexibility technologies. DIETER can capture multiple system benefits of electricity storage related to capacity, energy arbitrage and reserve provision.
​
DIETER is an open source model which may be freely used and modified by anyone. The code is licensed under the MIT License. Input data is licensed under the Creative Commons Attribution-ShareAlike 4.0 International Public License and available under http://www.diw.de/dieter. To view a copy of these licenses, visit http://opensource.org/licenses/MIT and http://creativecommons.org/licenses/by-sa/4.0/. Whenever you use this model, please refer to http://www.diw.de/dieter. We are happy to receive your feedback.
​
The model is implemented in the General Algebraic Modeling System (GAMS). Running the model thus requires a GAMS system, an LP solver, and respective licenses. We use the commercial solver CPLEX, but other LP solvers work, as well.