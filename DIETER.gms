
********************************************************************************
$ontext
The Dispatch and Investment Evaluation Tool with Endogenous Renewables (DIETER).
Version 1.5.0-pre, January 2021.
Written by Alexander Zerrahn, Wolf-Peter Schill, and Fabian St�ckl.
This work is licensed under the MIT License (MIT).
For more information on this license, visit http://opensource.org/licenses/mit-license.php.
Whenever you use this code, please refer to http://www.diw.de/dieter.
We are happy to receive feedback under azerrahn@diw.de and wschill@diw.de.
$offtext
********************************************************************************




**************************
***** GLOBAL OPTIONS *****
**************************

* Set star to skip Excel upload and load data from gdx
$setglobal skip_Excel ""

* Choose base year
$setglobal base_year "'2050'"

* Germany only - also adjust Excel inputs!
$setglobal GER_only "*"

* Set star to activate options
$setglobal DSM ""

$setglobal reserves_endogenous ""
$setglobal reserves_exogenous ""

$setglobal prosumage ""

$setglobal heat ""

* Set star to activate P2H2 module (The two sub-modules: "re-conversion" and "H2 production and distribution for traffic" are controlled in more detail via the data_input Excel file!)
$setglobal P2H2 "*"

$setglobal EV ""
$setglobal EV_EXOG ""
* Set star to indicate renewables constraint on electric vehicles - DEFAULT is same quota as for the rest of the electricity system
$setglobal EV_DEFAULT ""
$setglobal EV_100RES ""
$setglobal EV_FREE ""

* Set star to select run-of-river options either as exogenous parameter or as endogenous variable including reserve provision:
* if nothing is selected, ROR capacity will be set to zero
* if parameter option is selected, set appropriate values in fix.gmx
* if variable option is selected, set appropriate bound in data_input excel
$setglobal ror_parameter ""
$setglobal ror_variable ""

* Set star for no crossover to speed up calculation time by skipping crossover in LP solver
$setglobal no_crossover ""

$setglobal Simplified_transporation ""
$setglobal Time_consuming_transportation "*"

********************************************************************************

* Definition of strings for report parameters and sanity checks
* (Do not change settings below)
$setglobal sec_hour "1"

$setglobal reserves "%reserves_endogenous%%reserves_exogenous%"

* Sanity checks
$if "%ror_parameter%" == "*" $if "%ror_variable%" == "*" $abort Choose appropriate ROR option! ;

$if "%reserves%" == "**" $abort Choose only one reserve option (endogenous or exogenous)!

$if "%EV%" == "" $if "%EV_EXOG%" == "*" $abort Switch on EV! ;

$if "%EV%" == "*" $if "%EV_DEFAULT%%EV_100RES%%EV_FREE%" == "" $abort Choose exactly one EV option! ;
$if "%EV%" == "*" $if "%EV_DEFAULT%%EV_100RES%%EV_FREE%" == "**" $abort Choose exactly one EV option! ;
$if "%EV%" == "*" $if "%EV_DEFAULT%%EV_100RES%%EV_FREE%" == "***" $abort Choose exactly one EV option! ;

$if "%EV_EXOG%" == "*" $if "%EV_DEFAULT%%EV_100RES%%EV_FREE%" == "" $abort Choose exactly one EV option! ;
$if "%EV_EXOG%" == "*" $if "%EV_DEFAULT%%EV_100RES%%EV_FREE%" == "**" $abort Choose exactly one EV option! ;
$if "%EV_EXOG%" == "*" $if "%EV_DEFAULT%%EV_100RES%%EV_FREE%" == "***" $abort Choose exactly one EV option! ;

$if "%P2H2%" == "*" $if "%Simplified_transporation%" == "%Time_consuming_transportation%" $abort Choose one but only one type of transportation - either simplified or time consuming! ;




********************************************************************************

**************************
***** SOLVER OPTIONS *****
**************************

options
optcr = 0.00
reslim = 10000000
lp = cplex
mip = cplex
nlp = conopt
limrow = 0
limcol = 0
;

options
dispwidth = 15
limrow = 0
limcol = 0
solprint = off
sysout = off
;




********************************************************************************

**************************
***** Dataload *****
**************************

$include dataload.gms

%P2H2%$ontext

* sanity checks
abort$( sum( (n,h2_channel) , h2_recon_sw(n,h2_channel) + h2_mobility_sw(n,h2_channel) + h2_p2x_sw(n,h2_channel) ) / ( card(n) * card(h2_channel) ) > 1 ) "Only 'h2_mobility_sw' or 'recon_sw' or 'p2x_sw' can be chosen, but not two or more of them at the same time."
abort$( sum ( n , 1$( h2_tech_avail_sw(n,'PEM') = 0 AND h2_channel_avail_sw(n,'fuel_decent') = 1  ) ) > 0 ) "Using the channel 'fuel_decent' requires the electrolysis technology 'PEM'."
abort$( sum ( n , 1$( h2_channel_avail_sw(n,'fuel_decent') = 1 AND ( h2_recon_sw(n,'fuel_decent') = 1 OR h2_p2x_sw(n,'fuel_decent') ) ) ) > 0 ) "The channel 'fuel_decent' cannot be used for reconversion or p2x."
abort$( sum ( n , sum( h2_channel_wo_decent_set , 1$( h2_sto_p_type(n,h2_channel_wo_decent_set) = h2_sto_p_type(n,'fuel_decent') ) ) ) > 0 ) "The channel 'fuel_decent' must use a different storage type (number-identifier) as storage cannot be shared with other supply chains."


abort$(
sum( n , sum( h2_tech , sum( h2_tech_recon , 1$( h2_bidirect_sw(n,h2_tech_recon) AND h2_tech_recon_avail_set(n,h2_tech_recon) AND h2_tech_avail_set(n,h2_tech) AND sameAs(h2_tech,h2_tech_recon) ) ) ) )
<>
sum( n , sum( h2_tech_recon , 1$( h2_tech_recon_avail_set(n,h2_tech_recon) AND h2_bidirect_sw(n,h2_tech_recon) ) ) )
) "The (available) bidirectional electrolysis and reconversion technologies do not match."


$ontext
$offtext
*$stop




********************************************************************************

*************************************
***** Features for single nodes *****
*************************************

Set
features /dsm, ev, reserves, prosumage, rsvr_outflow, heat, P2H2/
;


Table
feat_node(features,n)
                    DE
%DSM%$ontext
dsm                 0
$ontext
$offtext
%reserves%$ontext
reserves            0
$ontext
$offtext
%ev%$ontext
ev                  0
$ontext
$offtext
%prosumage%$ontext
prosumage           0
$ontext
$offtext
rsvr_outflow        0
%heat%$ontext
heat                0
$ontext
$offtext
%P2H2%$ontext
P2H2                1
$ontext
$offtext
;


%DSM%$ontext
m_dsm_cu(n,dsm_curt)$(feat_node('dsm',n) = 0) = 0 ;
m_dsm_shift(n,dsm_shift)$(feat_node('dsm',n) = 0) = 0 ;
$ontext
$offtext

%prosumage%$ontext
m_res_pro(n,res)$(feat_node('prosumage',n) = 0) = 0 ;
m_sto_pro_e(n,sto)$(feat_node('prosumage',n) = 0) = 0 ;
m_sto_pro_p(n,sto)$(feat_node('prosumage',n) = 0) = 0 ;
$ontext
$offtext

phi_rsvr_min(n) = 0
;

%heat%$ontext
dh(n,bu,ch,h)$(feat_node('heat',n) = 0) = 0 ;
d_dhw(n,bu,ch,h)$(feat_node('heat',n) = 0) = 0 ;
$ontext
$offtext


Set
map_n_tech(n,tech)
map_n_sto(n,sto)
map_n_rsvr(n,rsvr)
map_n_dsm(n,dsm)
map_n_ev(n,ev)
map_l(l)
map_n_sto_pro(n,sto)
map_n_res_pro(n,tech)
;

map_n_tech(n,tech) = yes$m_p(n,tech) ;
map_n_sto(n,sto) = yes$m_sto_p_in(n,sto) ;
map_n_rsvr(n,rsvr) = yes$m_rsvr_p_out(n,rsvr) ;
map_n_dsm(n,dsm_curt) = yes$m_dsm_cu(n,dsm_curt) ;
map_n_dsm(n,dsm_shift) = yes$m_dsm_shift(n,dsm_shift) ;
map_n_ev(n,ev) = yes$ev_data(n,ev,'share_ev') ;
map_l(l) = yes$m_ntc(l) ;
map_n_sto_pro(n,sto) = yes$(yes$m_sto_pro_p(n,sto)) ;
map_n_res_pro(n,res) = yes$(yes$m_res_pro(n,res)) ;
;




********************************************************************************

***************************
***** Initialize data *****
***************************

* Parameters for default base year
d(n,h) = d_y(n,%base_year%,h) ;
phi_res(n,res,h) = phi_res_y(n,%base_year%,res,h) ;
rsvr_in(n,rsvr,h) = rsvr_in_y(n,%base_year%,rsvr,h) ;
phi_reserves_call(n,reserves,h) = phi_reserves_call_y(n,%base_year%,reserves,h) ;
phi_mean_reserves_call(n,reserves) = phi_mean_reserves_call_y(n,%base_year%,reserves) ;
reserves_exogenous(n,reserves,h) = reserves_exogenous_y(n,%base_year%,reserves,h) ;
dh(n,bu,ch,h) = dh_y(n,%base_year%,bu,ch,h) ;
d_dhw(n,bu,ch,h) = d_dhw_y(n,%base_year%,bu,ch,h) ;

dh(n,bu,ch,h) = area_floor(n,bu,ch) * dh(n,bu,ch,h) ;
d_dhw(n,bu,ch,h) = area_floor(n,bu,ch) * d_dhw(n,bu,ch,h) ;


* No interconnections between non-adjacent or nonuploaded nodes
m_ntc(l)$( smax(n,inc(l,n)) = 0 OR smin(n,inc(l,n)) = 0 ) = 0 ;


* Set parameters to default zero
phi_min_res = 0 ;
ev_quant = 0 ;
phi_pro_self = 0 ;
phi_pro_load(n) = 0 ;
phi_pro_load(n)$feat_node('prosumage',n) = 0.2 ;


Parameter
phi_min_res_exog(n)
min_flh(n,rsvr)
;

phi_min_res_exog(n) = 1 ;
min_flh(n,rsvr) = 0 ;




********************************************************************************
***** Model *****
********************************************************************************

$include model.gms




********************************************************************************
***** Options, fixings, report preparation *****
********************************************************************************

* Solver options
$onecho > cplex.opt
lpmethod 4
threads 4
epgap 1e-3
$offecho

%no_crossover%$ontext
$onecho > cplex.opt
lpmethod 4
threads 4
epgap 1e-3
barcrossalg -1
barepcomp 1e-8
$offecho
$ontext
$offtext

dieter.OptFile = 1;
dieter.holdFixed = 1 ;




********************************************************************************
***** Solve *****
********************************************************************************

* Inclusion of scenario and fixing
$include scenario.gms
$include fix.gms
solve DIETER using lp min Z


* Reporting
$include report.gms


execute_unload "results", report, report_tech, report_node, report_line, report_tech_hours, report_hours, report_cost
%prosumage%$ontext
, report_prosumage, report_prosumage_tech, report_prosumage_tech_hours, report_market, report_market_tech, report_market_tech_hours
$ontext
$offtext
%heat%$ontext
, report_heat_tech_hours, report_heat_tech
$ontext
$offtext
%reserves_endogenous%$ontext
, report_reserves, report_reserves_tech, report_reserves_tech_hours
$ontext
$offtext
%reserves_exogenous%$ontext
, report_reserves_tech, report_reserves_tech_hours, report_reserves_hours
$ontext
$offtext
;


%P2H2%$ontext

execute_unload "h2_output_data.gdx",
h2_report_electrolysis_cap
h2_report_recon_cap
h2_report_aux_cap_tech2channel
h2_report_aux_cap_channel2tech
h2_report_infrastructure_cap_channel
h2_report_h2mobiliy_shares
h2_report_electrolysis_flows
h2_report_bypass_1_flow
h2_report_bypass_2_flow
h2_report_prod_sto_level
h2_report_LP_sto_level
h2_report_MP_sto_level
h2_report_HP_sto_level
h2_other_flows
h2_report_filling_flow
h2_report_recon_flows
h2_fixed_costs_electrolysis_aux
h2_fixed_costs_reconversion
h2_fixed_costs
h2_fixed_costs_oa_by_channel
h2_elec_dem_electrolysis_aux_per_h
h2_elec_dem_reconversion_per_h
h2_elec_dem_per_h
h2_elec_dem_oa_by_channel_per_h
h2_elec_dem_electrolysis_aux
h2_elec_dem_reconversion
h2_elec_dem
h2_elec_dem_oa_by_channel
h2_elec_dem_costs_electrolysis_aux_per_h
h2_elec_dem_costs_reconversion_per_h
h2_elec_dem_costs_per_h
h2_elec_dem_costs_oa_by_channel_per_h
h2_elec_dem_costs_oa_by_channel
h2_add_costs_by_channel
h2_trans_on_road
;

$ontext
$offtext



* ---------------------------------------------------------------------------- *
* ---------------------------------------------------------------------------- *
* ---------------------------------------------------------------------------- *
